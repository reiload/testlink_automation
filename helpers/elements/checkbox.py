from selenium.webdriver.support.ui import Select


class CheckBox(object):

    def __set__(self, driver, attribute, value):

        test = ""

        if attribute == "index":
            test = Select(driver.find_element_by_tag_name("select")).select_by_index(value)
        if attribute == "value":
            test = Select(driver.find_element_by_tag_name("select")).select_by_value(value)
        if attribute == "text":
            test = Select(driver.find_element_by_tag_name("select")).select_by_visible_text(value)

        return test

    def __get__(self, driver, attribute, value):

        test = ""

        if attribute == "index":
            test = Select(driver.find_element_by_tag_name("select")).select_by_index(value)
        if attribute == "value":
            test = Select(driver.find_element_by_tag_name("select")).select_by_value(value)
        if attribute == "text":
            test = Select(driver.find_element_by_tag_name("select")).select_by_visible_text(value)

        return test.is_selected()
