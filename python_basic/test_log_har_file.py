from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# fp = webdriver.FirefoxProfile()
# fp.add_extension(extension='C:\\temp\\har_export_trigger-0.5.0-beta.10-fx.xpi')
# fp.set_preference("extensions.netmonitor.har.enableAutomation", True)
# fp.set_preference("extensions.netmonitor.har.contentAPIToken", "test1234")
# fp.set_preference("extensions.netmonitor.har.autoConnect", True)
# fp.set_preference("devtools.netmonitor.har.enableAutoExportToFile", True)
# fp.set_preference("devtools.netmonitor.har.compress", False)
# fp.set_preference("devtools.netmonitor.har.defaultFileName", "HarOutputExport_%y%m%d_%H%M%S")
# fp.set_preference("devtools.netmonitor.har.defaultLogDir", "C:\\temp")
# fp.set_preference("devtools.netmonitor.har.enableAutoExportToFile", True)
# fp.set_preference("devtools.netmonitor.har.forceExport", True)
def _waitForAlert(driver):
    return WebDriverWait(driver, 5).until(EC.alert_is_present())
#
# browser = webdriver.Firefox(firefox_profile=fp)
# browser.get("http://www.w3schools.com/js/tryit.asp?filename=tryjs_confirm")
# browser.switch_to.frame("iframeResult")
# browser.find_element(by=By.CSS_SELECTOR, value="body > button").click()
# alert = _waitForAlert(browser)
# alert.accept()


from browsermobproxy import Server
server = Server("C:\\browsermob-proxy-2.1.2\\bin\\browsermob-proxy")
server.start()
proxy = server.create_proxy()

from selenium import webdriver
profile  = webdriver.FirefoxProfile()
profile.set_proxy(proxy.selenium_proxy())
driver = webdriver.Firefox(firefox_profile=profile)


proxy.new_har("google")
driver.get("http://www.google.co.uk")
print(proxy.har) # returns a HAR JSON blob

server.stop()
driver.quit()