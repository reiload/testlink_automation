import xlsxwriter
from modules.json_read import JsonClass
from time import strftime
from os import getcwd
class report_excel(object):


    def create_excel(self):
        print ("Create file Report.xlsx")
        name = 'Report_' +str(strftime('%Y%m%d%H%M'))+ str(".xlsx")
        PATH = "c:\{}".format(getcwd().split('\\')[1])
        FILE_PATH = ("{}\\reports\\{}".format(PATH, name))
        workbook = xlsxwriter.Workbook(FILE_PATH)
        worksheet1 = workbook.add_worksheet()
        currency_format = workbook.add_format({'num_format': '$#,##0'})

        # Create a new Chart object.
        chart = workbook.add_chart({'type': 'column'})
        # Some sample data for the table.
        print("Report excel file")
        obj = JsonClass("resultado.json")
        data = obj.get_scenario_data()
        countPassed, countFailed = 0, 0
        caption = 'Test Report TestLink Version 13.0'
        merge_format = workbook.add_format({
            'bold':     True,
            'border':   0,
            'align':    'left',
            'valign':   'vcenter',
            'color': 'blue',
            #'fg_color': '#D7E4BC',
        })
        # Set the columns widths.
        worksheet1.set_column('B:G', 20)
        worksheet1.insert_image('A1', "{}\\reports\\imgs\\{}".format(PATH, "testlink.png"), {'x_scale': 0.7, 'y_scale': 0.7,
                                'x_offset': 4, 'y_offset': 4})
        # Write the caption.
        worksheet1.write('B1', caption, merge_format)
        worksheet1.write('B2', "Total of Tests:")
        worksheet1.write('C2',(len(data)))
        worksheet1.write('D2', "Total Failed:")

        for x in range(len(data)):
            if data[x][1] == "passed": countPassed+=1
            elif data[x][1] == "failed": countFailed+=1

        worksheet1.write('E2', countFailed)
        worksheet1.write('D3', "Total Passed:")
        worksheet1.write('E3', countPassed)
        # Add a table to the worksheet.
        worksheet1.add_table('B6:C'+str(6+len(data)), {'data': data,
                                       'columns': [{'header': 'Test/Cenário'},
                                                   {'header': 'Status'}
                                                   ]})

        chart.add_series({
            'categories': '=Sheet1!$D$3:$D$3',
            'values':     '=Sheet1!$E$3:$E$3',
            'line':       {'color': 'blue'},
            'name': 'Passed',
            'fill':   {'color': '#00387b'},
        })

        # Configure the chart. In simplest case we add one or more data series.
        chart.add_series({
            'categories': '=Sheet1!$D$2:$D$2',
            'values':     '=Sheet1!$E$2:$E$2',
            'line':       {'color': 'red'},
            'name':     'Failed',
            'fill':   {'color': '#a52a2a'},
        })
        # Insert the chart into the worksheet.
        worksheet1.insert_chart('F6', chart)
        workbook.close()

        #report = sendReport()
        #report.ReportSanity(FILE_PATH,name)
if '__main__' == __name__:
    obj = report_excel()
    obj.create_excel()